import useSWR from 'swr';
import qs from 'querystring';

function fetcher(url) {
  return fetch(url).then(res => res.json());
}

function validateResponse(apiResponse) {
  return new Promise((resolve, reject) => {
    if (apiResponse.Response === 'True') {
      resolve(apiResponse);
    } else {
      reject(apiResponse.Error);
    }
  });
}

export const useGetMovie = (query, page) => {
  const shouldFetch = query && page;
  return useSWR(shouldFetch ? ['home/search', query, page] : null, async () => {
    const res = await fetcher(
      `${process.env.REACT_APP_SERVICE_URL}/?${qs.stringify({
        apiKey: process.env.REACT_APP_SERVICE_API_KEY,
        s: query,
        type: 'movie',
        page
      })}`
    );

    return validateResponse(res);
  });
};

export const useGetMovieDetails = imdbId => {
  return useSWR(['details/overview'], async () => {
    const res = await fetcher(
      `${process.env.REACT_APP_SERVICE_URL}?${qs.stringify({
        apiKey: process.env.REACT_APP_SERVICE_API_KEY,
        i: imdbId
      })}`
    );

    return validateResponse(res);
  });
};
