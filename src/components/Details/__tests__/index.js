import { render, fireEvent } from '@testing-library/react';
import { createMemoryHistory } from 'history';
import { Router } from 'react-router-dom';
import Details from '../index';
import { useGetMovieDetails } from '../../../hooks/request';

const mockSuccessResponse = {
  Title: 'Captain Marvel'
};

const mockHistoryPush = jest.fn();

jest.mock('../../../hooks/request');
jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    push: mockHistoryPush
  })
}));

const renderSubject = () => {
  const history = createMemoryHistory();
  return render(
    <Router history={history}>
      <Details />
    </Router>
  );
};

describe('Details', () => {
  beforeEach(() => {
    useGetMovieDetails.mockReturnValue({
      data: undefined,
      isValidating: false,
      error: undefined
    });
  });

  it('renders', () => {
    renderSubject();
  });

  it('should go back home page on back button clicked', () => {
    const { getByRole } = renderSubject();

    fireEvent.click(getByRole('button'));

    expect(mockHistoryPush).toHaveBeenCalledWith('/');
  });

  it('should display data on successful fetch', async () => {
    useGetMovieDetails.mockReturnValue({
      data: mockSuccessResponse
    });
    const { findByText } = renderSubject();

    await findByText(/Captain Marvel/);
  });

  it('should display loading message while validating', async () => {
    useGetMovieDetails.mockReturnValue({
      isValidating: true
    });

    const { findByText } = renderSubject();

    await findByText('Loading...');
  });

  it('should display error message on rejected response', async () => {
    useGetMovieDetails.mockReturnValue({
      error: true
    });

    const { findByText } = renderSubject();

    await findByText('Error loading data');
  });
});
