import styled from 'styled-components';
import { useHistory, useParams } from 'react-router-dom';
import { useGetMovieDetails } from '../../hooks/request';

const Details = () => {
  const { push } = useHistory();
  const { id } = useParams();
  const { data, isValidating, error } = useGetMovieDetails(id);

  return (
    <Container>
      <Button onClick={() => push('/')}>Back</Button>
      <div>
        {error ? (
          'Error loading data'
        ) : isValidating ? (
          'Loading...'
        ) : (
          <pre>{JSON.stringify(data, null, 2)}</pre>
        )}
      </div>
    </Container>
  );
};

export default Details;

const Container = styled.div``;

const Button = styled.button`
  margin-bottom: 30px;
  padding: 2px 5px;
`;
