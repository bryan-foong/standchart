import styled from 'styled-components';
import { Link } from 'react-router-dom';
import media from '../../utils/media';

const MovieItem = ({ id, title, year, poster }) => {
  return (
    <Container>
      <Attributes>
        <Label>Title:</Label>
        <Value>
          <Link to={`/details/${id}`}>{title}</Link>
        </Value>
      </Attributes>
      <Attributes>
        <Label>Year:</Label>
        <Value>{year}</Value>
      </Attributes>
      <Attributes>
        <Label>Poster:</Label>
        <Value>
          <img src={poster} alt={title} />
        </Value>
      </Attributes>
    </Container>
  );
};

export default MovieItem;

const Container = styled.div`
  margin-bottom: 30px;
`;

const Attributes = styled.div`
  display: flex;
  margin-bottom: 5px;
`;

const Label = styled.div`
  flex: 1;
  font-weight: bold;
`;

const Value = styled.div`
  flex: 2;

  ${media.lessThan('sm')`
    flex: 3;
  `}
`;
