import { render, fireEvent } from '@testing-library/react';
import Home from '../index';
import { createMemoryHistory } from 'history';
import { Router } from 'react-router-dom';
import { useGetMovie } from '../../../hooks/request';

jest.mock('../../../hooks/request');

const mockSuccessResponse = {
  Search: [
    {
      Title: 'Captain Marvel',
      Year: '2019',
      imdbID: 'tt4154664',
      Type: 'movie',
      Poster:
        'https://m.media-amazon.com/images/M/MV5BMTE0YWFmOTMtYTU2ZS00ZTIxLWE3OTEtYTNiYzBkZjViZThiXkEyXkFqcGdeQXVyODMzMzQ4OTI@._V1_SX300.jpg'
    }
  ],
  totalResults: '1378',
  Response: 'True'
};

const mockErrorResponse = {
  Response: 'False',
  Error: 'Movie not found!'
};

const renderSubject = () => {
  const history = createMemoryHistory();
  return render(
    <Router history={history}>
      <Home />
    </Router>
  );
};

describe('Home', () => {
  beforeEach(() => {
    useGetMovie.mockReturnValue({
      data: undefined,
      isValidating: false,
      error: undefined
    });
  });

  it('renders', () => {
    renderSubject();
  });

  it('should return query result', async () => {
    useGetMovie.mockReturnValue({
      data: mockSuccessResponse
    });
    const { getByTestId, findByText } = renderSubject();

    const input = getByTestId('search-input');
    fireEvent.change(input, { target: { value: 'marvel' } });

    await findByText('Captain Marvel');
  });

  it('should display rejected response', async () => {
    useGetMovie.mockReturnValue({
      error: mockErrorResponse.Error
    });
    const { findByText } = renderSubject();

    await findByText(mockErrorResponse.Error);
  });
});
