import React, { useState } from 'react';
import styled from 'styled-components';
import { useGetMovie } from '../../hooks/request';
import InfiniteScroll from 'react-infinite-scroll-component';
import Item from './Item';
import { useDebounce } from 'use-debounce';

const Home = () => {
  const [list, setList] = useState([]);
  const [value, setValue] = useState('');
  const [page, setPage] = useState(1);
  const [searchValue] = useDebounce(value, 1000);
  const { data, error } = useGetMovie(searchValue, page);

  React.useEffect(() => {
    setList([]);
    setPage(1);
  }, [searchValue]);

  React.useEffect(() => {
    if (data) {
      setList(currentList => currentList.concat(data.Search));
    }
  }, [data]);

  const handleChange = e => {
    const { value } = e.target;
    setValue(value);
  };

  const loadMoreItems = () => {
    setPage(currentPage => (currentPage += 1));
  };

  const totalResults = data?.totalResults ? Number(data.totalResults) : 0;

  return (
    <Container>
      <Title>Movie Explorer</Title>
      <Input
        data-testid="search-input"
        placeholder="Search movie"
        value={value}
        onChange={handleChange}
      />
      {error ? (
        error
      ) : (
        <InfiniteScroll
          dataLength={list.length}
          next={loadMoreItems}
          hasMore={list.length < totalResults}
          loader="Loading..."
        >
          {list.map(i => (
            <Item
              key={i.imdbID}
              id={i.imdbID}
              title={i.Title}
              year={i.Year}
              poster={i.Poster}
            />
          ))}
        </InfiniteScroll>
      )}
    </Container>
  );
};

export default Home;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`;

const Title = styled.h2`
  text-align: center;
  margin-bottom: 20px;
`;

const Input = styled.input`
  height: 38px;
  margin-bottom: 30px;
  padding: 10px;
`;
