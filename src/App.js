import styled, { createGlobalStyle } from 'styled-components';
import AppRouter from './router';

const App = () => {
  return (
    <Container>
      <GlobalStyles />
      <AppRouter />
    </Container>
  );
};

export default App;

const Container = styled.div`
  max-width: 720px;
  margin: 0 auto;
  padding: 20px 0;
  height: 100%;
`;

const GlobalStyles = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }
  
  html, body, #root {
    height: 100%;
  }
`;
